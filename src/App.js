import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from "react-router-dom";
import Login from './Login';
import Dashboard from './Dashboard';
import qoreContext from './qoreContext';
import { AuthProvider } from './AuthContext';
import Profile from './Profile';

function App() {
  return (
    <qoreContext.context.Provider
      value={{
        client: qoreContext.client
      }}
    >
      <AuthProvider>
        <Router>
          <Switch>
            <Route path="/login" component={Login} />
            <Route path="/dashboard" component={Dashboard} />
            <Route path="/profile" component={Profile} />
            <Redirect from="/" to="/login" />
          </Switch>
        </Router>
      </AuthProvider>
    </qoreContext.context.Provider>
  );
}

export default App;
