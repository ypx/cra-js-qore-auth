import React from 'react'
import { Link } from 'react-router-dom';
import { AuthContext } from './AuthContext';

const Profile = () => {
  const { user, setUser } = React.useContext(AuthContext);
  const [val, setVal] = React.useState('');
  const onChange = (e) => {
    const value = e.currentTarget.value;
    setVal(value);
  };
  const submit = () => {
    setUser(val);
  }
  return (
    <div>
      <h1>Profile Page</h1>
      <Link to="/dashboard">go to Dashboard</Link>
      <input type="text" value={val} onChange={onChange} />
      <button onClick={submit}>Change name</button>
      <p>{user}</p>
    </div>
  )
}

export default Profile
