import { QoreClient } from "@feedloop/qore-client";
import createQoreContext from "@feedloop/qore-react";
import config from "./qore.config";
import schema from "./qore.schema";

const client = new QoreClient({
  ...config,
  getToken: () => window.localStorage.getItem("token"),
  onError: (error) => {
    if (error.response.status === 401) {
      window.location.href = '/login'
    }
  }
});
client.init(schema);

const qoreContext = createQoreContext(client);
export default qoreContext;