import React from 'react'
import { AuthContext } from './AuthContext';

const Login = () => {
  const { login } = React.useContext(AuthContext);
  const [form, setForm] = React.useState({
    email: '',
    password: ''
  });
  const onEmailChange = (e) => {
    const value = e.currentTarget.value;
    setForm(prev => ({ ...prev, email: value }))
  };
  const onPasswordChange = (e) => {
    const value = e.currentTarget.value;
    setForm(prev => ({ ...prev, password: value }))
  };
  const onSubmit = React.useCallback(async (e) => {
    e.preventDefault();
    await login({
      identifier: form.email,
      password: form.password,
    });
  }, [form, login]);
  return (
    <div>
      <h1>Login</h1>
      <form>
        <input type="text" value={form.email} placeholder="email" onChange={onEmailChange} />
        <input type="password" value={form.password} placeholder="password" onChange={onPasswordChange} />
        <button onClick={onSubmit}>login</button>
      </form>
    </div>
  )
}

export default Login
