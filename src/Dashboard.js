import React from 'react'
import { Link } from 'react-router-dom';
import { AuthContext } from './AuthContext';

const Dashboard = () => {
  const { user } = React.useContext(AuthContext);
  return (
    <div>
      <h1>Dashboard</h1>
      <Link to="/profile">go to Profile</Link>
      <p>{user}</p>
    </div>
  )
}

export default Dashboard
