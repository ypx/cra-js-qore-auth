import React from 'react';
import qoreContext from './qoreContext';

const initialValue = {
  user: undefined,
  setUser: () => {},
  login: async () => {}
}

const AuthContext = React.createContext(initialValue)

const useAuthContext = () => {
  const [user, setUser] = React.useState('Firaz');
  const client = qoreContext.useClient();
  const login = React.useCallback(async (data) => {
    try {
      const token = await client.authenticate(
        data.identifier,
        data.password
      );
      window.localStorage.setItem('token', token);
      window.location.href = '/dashboard'
    } catch (e) {
      if (e.response.status === 401) {
        window.location.href = '/login'
      }
    }
  }, [client])
  return { user, setUser, login }
}

const AuthProvider = (props) => {
  const { Provider } = AuthContext;
  const value = useAuthContext();
  return (
    <Provider value={value}>
      {props.children}
    </Provider>
  )
}

export { AuthProvider, AuthContext }
